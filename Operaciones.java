public class Operaciones{

private int a,b;

// sobre carga de constructores
 public Operaciones(){
 a=2;
 b=3;
 }
public Operaciones(int a, int b){
this.a = a;
this.b = b;
}

public int getA(){
return this.a;
}
public int getB(){
return this.b;
}

// sobrecarga de metodo  suma()
public int suma(int a, int b){
return a+b;
}
public double suma(double a , double b){
return a+b;
}
public int suma(int a, int b, int c){
return a+b+c;
}
}